$(document).on('click', '#menuToggle', function () {
    $('.dark-bg').fadeIn('fast');
    $('.main-menu-items').addClass('show');
});
$(document).on('click', '.dark-bg', function () {
    $(this).fadeOut('fast');
    $('.main-menu-items').removeClass('show');
});

function mainColor() {
   localStorage.setItem("color", "main");
   window.location.reload();
}
function purpleColor() {
  localStorage.setItem("color", "purple");
  window.location.reload();
}
function berryColor() {
   localStorage.setItem("color", "berry");
   window.location.reload();
}

var colorObject =  localStorage.getItem('color');

if(colorObject == "main" ){
    console.log('main color')
    document.documentElement.style.setProperty('--colorMain', '#1dc690');
    document.documentElement.style.setProperty('--colorMainTransparent', 'rgba(29, 198, 144, 0.62)');
    document.documentElement.style.setProperty('--colorSecondary', '#c6391d');
    document.documentElement.style.setProperty('--colorCopyright', '#21b183');
}
if(colorObject == "purple" ){
    console.log('purple color')
    document.documentElement.style.setProperty('--colorMain', '#6f42c1');
    document.documentElement.style.setProperty('--colorMainTransparent', 'rgba(111, 66, 193, 0.62)');
    document.documentElement.style.setProperty('--colorSecondary', '#000');
    document.documentElement.style.setProperty('--colorCopyright', '#563495');
}
if(colorObject == "berry" ){
    console.log('berry color')
    document.documentElement.style.setProperty('--colorMain', '#db277c');
    document.documentElement.style.setProperty('--colorMainTransparent', 'rgba(219, 39, 124, 0.62)');
    document.documentElement.style.setProperty('--colorSecondary', '#5b5b5b');
    document.documentElement.style.setProperty('--colorCopyright', '#B53471');
}




$(document).ready(function () {
    $('#birth-date').mask('0000/00/00');
    $('#phone-number').mask('0000-000-0000');
});